package shapes2d;

import shapes3d.Cube;
import shapes3d.Cylinder;

public class Main {

    public static void main(String[] args) {
        Circle circle = new Circle(1);
        System.out.println(circle);
        Cylinder cylinder = new Cylinder(1,1);
        System.out.println(cylinder);
        Cube cube = new Cube(1);
        System.out.println(cube);
        Square square = new Square(1);
        System.out.println(square);
    }
}

package shapes2d;

public class Square {
    public double x;
    public Square(double x){
        this.x=x;
    }
    public double area(){
        return x*x;
    }

    @Override
    public String toString() {
        return "Square{" +
                "x=" + x + " area = "+ area()+
                '}';
    }
    /*public static void main(String[] args) {
        Square square = new Square(3);
        System.out.println(square);

    }*/
}

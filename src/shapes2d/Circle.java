package shapes2d;

public class Circle {

    public double r;

    public Circle(double r) {
        this.r = r;
    }

    public double area() {
        return Math.PI * r * r;
    }

    @Override
    public String toString() {
        return "Circle{" +
                "r=" + r + " area = "+ area()+
                '}';
    }

    /*public static void main(String[] args) {
       Circle circle = new Circle(2);
        System.out.println(circle.area());
    }*/
}

package shapes3d;

import shapes2d.Square;

public class Cube extends Square {
    //Square square = new Square(3);
    public Cube(double x) {
        super(x);
    }
    public double CubeArea(){
        return 6*super.area();
    }
    public double CubeVolume(){
        return super.x* super.area();
    }

    @Override
    public String toString() {
        return "Cube{" +
                "x= " + x + " Volume = "+ CubeVolume()+ " Area = "+ CubeArea()+
                '}';
    }

    /*public static void main(String[] args) {
        Cube cube = new Cube(1);
        System.out.println(cube);

    }*/
}

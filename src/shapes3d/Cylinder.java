package shapes3d;

import shapes2d.Circle;

public class Cylinder extends Circle {
    //Circle circle = new Circle(3);
    public double length;

    public Cylinder(double r,double length) {
        super(r);
        this.length = length;
    }


    public double Volume(){
        return super.area()*length;
    }
    public double CylinderArea(){
        //Circle circle = new Circle(2);
        return super.area()*2*super.r+2*length/ super.r;
    }

    @Override
    public String toString() {
        return "Cylinder{" +
                "length= " + length + " area = "+CylinderArea()+" volume = "+ Volume()+
                '}';
    }
    /*public static void main(String[] args) {
        Cylinder cylinder = new Cylinder(3,2);
        System.out.println(cylinder);
    }*/


    }
